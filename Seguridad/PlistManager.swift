//
//  PlistManager.swift
//  Seguridad
//
//  Created by Salvador Lopez on 27/06/23.
//

import Foundation

class PlistManager {
    
    static let shared = PlistManager()
    
    private init(){
        // Init
    }
    
    func leerPlist(name:String, completion: @escaping ([String]?) -> Void){
        guard let path = Bundle.main.path(forResource: name, ofType: "plist"),
              let xml = FileManager.default.contents(atPath: path) else {
            completion(nil)
            return
        }
        
        let result = try? PropertyListSerialization.propertyList(from: xml, options:.mutableContainersAndLeaves, format: nil) as? [String]
        completion(result)
    }
    
    func leerPlistWithValue(name:String, value: String, completion: @escaping (String?) -> Void){
        guard let path = Bundle.main.path(forResource: name, ofType: "plist"),
              let xml = FileManager.default.contents(atPath: path),
              let data = try? PropertyListSerialization.propertyList(from: xml, options: .mutableContainersAndLeaves, format: nil),
              let plistValue = data as? [String] else {
            completion(nil)
            return
        }
        var result = plistValue.filter { val in
            return val == value
        }
        completion(result.first)
    }
    
}
