//
//  Practica.swift
//  Seguridad
//
//  Created by Salvador Lopez on 27/06/23.
//

import Foundation

//MARK: PRACTICA 3

/**
 Practica: 3
 
 Objetivo: Reforzar el uso de los metodos canEvaluatePolicy y evaluatePolicy de la clase LAContext de la librería Local Autentication.
 
 Requerimientos:
 
 Partiendo de la app de medicamentos correspondiente a la practica numero 2, realice una seccion de login previo a la lista de medicamentos.

 Para esta nueva vista de login, considere que el usuario pueda acceder colocando valores de usuario y password en TextFileds, y de manera adicional mostrar un boton que me permita ingresar con una autenticacion biometrica.
 
 Despues de evaluar la autenticacion con el simulador de manera exitosa, el usuario será redirigido a la lista de medicamentos de la practica 2.
 
 Considere:

 Realizar una clase que me permita crear una sola instancia con el patron de diseño Singleton llamada BiometricAuthManager y que contenga un metodo publico de autenticacion.
 
 **/
