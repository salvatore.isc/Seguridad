//
//  ViewController2.swift
//  Seguridad
//
//  Created by Salvador Lopez on 28/06/23.
//

import UIKit

class ViewController2: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //MARK: INSERT KEYCHAIN
        /*if let result = KeychainManager.crearKeychainItem(username: "jose", service: "my_service_app", password: "qwerty123"){
            print("Item creado con exito: \(result)")
        }*/
        
        //MARK: READ KEYCHAIN
        /*if let passKeychain = KeychainManager.leerKeychainItem(username: "jose", service: "my_service_app"){
            print("Password obtenido desde el keychain: \(passKeychain)")
        }*/
        
        /*if let facade = KeychainFacade().createKeyItem(user: "test", service: "test_service", pass: "1234"){
            print("Item creado con exito: \(facade)")
        }else{
            print("Error en la clase Facade")
        }*/
        
        if let facadeResponse = KeychainFacade().readKeyItem(user: "test", service: "test_service"){
            print("Password obtenido desde el keychain con Facade: \(facadeResponse)")
        }else{
            print("Error en lectura de la clase Facade")
        }
        
        
    }

}
