//
//  KeychainManager.swift
//  Seguridad
//
//  Created by Salvador Lopez on 28/06/23.
//

import Foundation

class KeychainManager{
    
    init(){
        print("Se creo una instancia de KeychainManager")
    }
    
    func crearKeychainItem(username: String, service: String, password: String) -> [String:Any]? {
        
        // Crear item a partir de un Dic
        var dict = [String:AnyObject]()
        
        // Colocando la llave para un password generico.
        dict[kSecClass as String] = kSecClassGenericPassword
        // El pass debe ser accesible cuando el dispositivo esta desbloqueado.
        dict[kSecAttrAccessible as String] = kSecAttrAccessibleWhenUnlocked
        // Llave para la etiqueta que ayudara a encontrar el item
        dict[kSecAttrLabel as String] = service as CFString
        // Llave para colocar el nombre del usuario
        dict[kSecAttrAccount as String] = username as CFString
        // Llave para el servicio
        dict[kSecAttrService as String] = service as CFString
        // Coloca la llave que almacenara el pass en una cadena de bytes.
        dict[kSecValueData as String] = password.data(using: .utf8)! as CFData
        // Llave para establecer la lectura de los atributos
        dict[kSecReturnAttributes as String] = kCFBooleanTrue
        
        // Agregar el item a keychain
        var result: AnyObject?
        let status = withUnsafeMutablePointer(to: &result) {
            SecItemAdd(dict as CFDictionary, UnsafeMutablePointer($0))
        }
        
        if status == errSecSuccess {
            if let attrResult = result as? [String:AnyObject]{
                return attrResult
            }
        }else{
            print("Error al agregar el item a Keychain: \(status)")
        }
        
        return nil
        
    }
    
    func leerKeychainItem(username: String, service: String) -> String? {
        // Crear Dic para consulta
        var dict = [String:AnyObject]()
        
        // Tipo de item que queremos recuperar
        dict[kSecClass as String] = kSecClassGenericPassword
        // Etiqueta
        dict[kSecAttrLabel as String] = service as CFString
        // Nombre de usuario
        dict[kSecAttrAccount as String] = username as CFString
        // Servicio
        dict[kSecAttrService as String] = service as CFString
        // Retorno de los atributos
        dict[kSecReturnData as String] = kCFBooleanTrue
        
        // Lanzar consulta
        var queryResponse: AnyObject?
        let status = withUnsafeMutablePointer(to: &queryResponse) {
            SecItemCopyMatching(dict as CFDictionary, UnsafeMutablePointer($0))
        }
        
        if status != 0 {
            print("Error al encontrar el item de Keychain: \(status)")
        }else{
            if let passwordData = queryResponse as? Data,
               let password = String(data:passwordData,encoding: .utf8){
                return password
            }
        }
        
        return nil
    }
    
}
