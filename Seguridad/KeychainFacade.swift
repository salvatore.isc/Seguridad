//
//  KeychainFacade.swift
//  Seguridad
//
//  Created by Salvador Lopez on 28/06/23.
//

import Foundation

class KeychainFacade {
    
    private let keyManager = KeychainManager()
    
    init(){
        print("Se creo una instancia del Facade")
    }
    
    func createKeyItem(user: String, service: String, pass: String) -> [String:Any]?{
        return keyManager.crearKeychainItem(username: user, service: service, password: pass)
    }
    
    func readKeyItem(user:String,service:String) -> String? {
        return keyManager.leerKeychainItem(username: user, service: service)
    }
    
}
