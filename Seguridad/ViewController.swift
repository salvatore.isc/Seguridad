//
//  ViewController.swift
//  Seguridad
//
//  Created by Salvador Lopez on 27/06/23.
//

import UIKit
import LocalAuthentication

class ViewController: UIViewController {
    
    let button = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //ConfButton
        button.frame = CGRect(x: (UIScreen.main.bounds.width/2)-50, y: UIScreen.main.bounds.height/2, width: 100, height: 35)
        button.setTitle("Login", for: .normal)
        button.addTarget(self, action: #selector(authAction), for: .touchUpInside)
        button.backgroundColor = .blue
        self.view.addSubview(button)
        
    }
    
    @objc func authAction() {
        // obj para poder auth
        let context = LAContext()
        var error: NSError?
        var userText = "Se necesita validar su identidad"
        // comprobar si podemos acceder a la funcionalidad biometrica
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error){
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: userText){
                success, error in
                if error == nil{
                    AlertController.showAlert(title: "Login", message: "Autenticacion con exito", butttons: ["Aceptar"]) { opt in
                        // Success login
                        
                        let serverUrl = Bundle.main.object(forInfoDictionaryKey: "ServerUrl") as? String
                        print(serverUrl)
                        
                        PlistManager.shared.leerPlist(name: "Frutas") { frutas in
                            if let frutas = frutas {
                                print(frutas)
                            }
                        }
                        
                        PlistManager.shared.leerPlistWithValue(name: "Frutas", value: "Uva") { result in
                            if let result = result{
                                print("Se encontro la configuracion: \(result)")
                            }else{
                                print("No se encontro el valor en el PLIST")
                            }
                        }
                        
                    }
                }else{
                    if let error = error as NSError?{
                        switch error.code{
                        case LAError.systemCancel.rawValue:
                            AlertController.showAlert(title: "Error en la autenticacion", message: "La autenticacion a sido cancelada por el sistema", butttons: ["Aceptar"]) { opt in
                                // Do something...
                            }
                        case LAError.userCancel.rawValue:
                            AlertController.showAlert(title: "Error en la autenticacion", message: "El usuario a cancelado la autenticacion.", butttons: ["Aceptar"]) { opt in
                                // Do something...
                            }
                        case LAError.userFallback.rawValue:
                            AlertController.showAlert(title: "Error en la autenticacion", message: "Se ha elegido otro metodo de autenticacion", butttons: ["Aceptar"]) { opt in
                                // Do something...
                            }
                        default: break
                        }
                    }
                }
            }
        }else{
            // No contamos con recursos biometrico
            if let error = error {
                switch error.code{
                case LAError.biometryLockout.rawValue:
                    AlertController.showAlert(title: "Error en datos biometricos", message: "El biometrico esta bloqueado debido a muchos intentos", butttons: ["Aceptar"]) { option in
                        // Do something...
                    }
                case LAError.passcodeNotSet.rawValue:
                    AlertController.showAlert(title: "Error en datos biometricos", message: "No se ha configurado el pin", butttons: ["Aceptar"]) { option in
                        // Do something...
                    }
                default: AlertController.showAlert(title: "Error en datos biometricos", message: "Error desconocido...", butttons: ["Aceptar"]) { opt in
                    // Do something
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                }
                }
            }
        }
        
    }


}

